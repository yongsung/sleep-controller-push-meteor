Tasks = new Mongo.Collection('tasks');

Tasks.schema = new SimpleSchema({
    requester: {type: String},
    task: {type: String},
    taskLocation: {type: String},
    taskDestination: {type: String},
    deadline: {type: String}
});
