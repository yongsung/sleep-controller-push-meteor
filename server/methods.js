var count = 0;
var didNotified = false;
Meteor.methods({
    storeTask: function(arg) {
        console.log(arg);
        Tasks.insert({requester:arg.requester, task: arg.task, taskLocation: arg.taskLocation, taskDestination: arg.taskDestination, deadline: arg.deadline});
        return "Yes";
        // return Meteor.LocationData.find().fetch();
    },

    sendNotification: function() {
        // return Meteor.LocationData.find().fetch();
        var apn = Meteor.npmRequire("apn");
        var options = {
            // cert: "/Users/yk/Projects/otg-meteor/private/dev-cert/cert.pem",
            // key: "/Users/yk/Projects/otg-meteor/private/dev-cert/key.pem",
            cert: "/Users/yk/Projects/otg-meteor/private/prod-cert/prod_cert.pem",
            key: "/Users/yk/Projects/otg-meteor/private/prod-cert/prod_key.pem",
            passphrase: "yongsung",
            production: "true" //working with production certificate!!!!
        };
        var apnError = function(err){
            console.log("APN Error:", err);
        }
        options.errorCallback = apnError;
        var apnConnection = new apn.Connection(options);
        // var myDevice = new apn.Device("bbdb453ef8edba04c4165d6e264d06f0709deef52846fbb84f80253fa8a6462d");
        var myDevice = new apn.Device("df46a83a109e33c40421f168bb6020a59f5358b3ad500a8c047608ff24e03e39");
        // 8332cb0b0de2415bbb963457582c0546
        var note = new apn.Notification();
        note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
        note.badge = 1;
        note.sound = "ping.aiff";
        note.alert = "Sleep Controller: Using cellphone before go to bed might affect your sleep quality!";
        note.payload = {'messageFrom': 'Caroline'};
        apnConnection.pushNotification(note, myDevice);
    },

    getTask: function() {
        return Tasks.find().fetch();
    },

    storeUserInfo: function(arg) {
        Requesters.insert({tokenId: arg.tokenId});
    },

    storeLocation: function(arg) {
        Locations.insert({location: arg.location, userId: arg.userId});
        console.log(parseFloat(arg.location));
        if (parseFloat(arg.location) <= 5) {
            count++;
            console.log(count);
            if (count>=20 ) {
                // didNotified = true;
                Meteor.call('sendNotification');
                count=0;
            }
        }
    }
});




