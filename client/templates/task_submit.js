Template.taskSubmit.events({
    'submit form': function(e) {
        e.preventDefault();

        var task = {
            name: $(e.target).find('[name=task]').val(),
            location: $(e.target).find('[name=location]').val()
        };
        task._id = Tasks.insert(task);
        Router.go('taskPage', task);
    }
});

Template.taskSubmit.helpers({
    // Router.route('/submit', {name: 'postSubmit'});
});
